import * as React from 'react';
import { Helmet } from "react-helmet";
import Footer from '@/components/Footer';
import Header from '@/components/Header';
import App from '../components/App';
import { Layout } from 'antd';
import { config } from '../global';

interface Props {
  title?: string;
}

export default class BasicLayout extends React.Component<Props, {}>{

  render() {
    let title = config.siteName; //default title
    if (this.props.title) {
      title = `${this.props.title} - ${config.siteName}`;
    }
    return (
      <App>
        <Helmet>
          <title>{title}</title>
        </Helmet>
        <Layout>
          <Header />
          {this.props.children}
          <Footer />
        </Layout>
      </App>
    );
  }
}
