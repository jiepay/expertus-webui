import * as React from 'react';
import { Drawer, Menu, Button, Row, Col, Icon } from 'antd';
import Link from 'umi/link';
import logo from '../assets/expertus.svg';
import burgerIcon from '../assets/hamburger.svg';

interface Props { }
interface State {
  visible: boolean;
}

export default class DrawerMenu extends React.Component<Props,State> {
  state = { visible: false };

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };

  render() {
    const menu = (
      <Menu mode="inline" style={{ width: '100%', border: 'none', textTransform: 'uppercase' }}>
        <Menu.Item key="0">
          <a href="#hello">Hello</a>
        </Menu.Item>
        <Menu.Item key="1">
          <a href="#profile">Profile</a>
        </Menu.Item>
        <Menu.Item key="2">
          <a href="#finance">Finance</a>
        </Menu.Item>
      </Menu>
    );
    return (
      <div>
        <Button onClick={this.showDrawer} style={{ height: 64, border: 'none', boxShadow: 'none' }}>
          <img src={burgerIcon} />
        </Button>
        <Drawer
          className="nav-drawer"
          title={
            <Row type="flex">
              <Col md={12}>
                <Button onClick={this.onClose} style={{ height: '100%', border: 'none' }}>
                  <Icon type="close" style={{ fontSize: '1.8em' }} />
                </Button>
              </Col>
              <Col md={12}>
                <Link to="/"><img src={logo} /></Link>
              </Col>
            </Row>
          }
          placement="left"
          closable={false}
          onClose={this.onClose}
          visible={this.state.visible}
        >
        {menu}
      </Drawer>
      </div>
    );
  }
}
