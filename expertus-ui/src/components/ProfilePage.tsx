import * as React from 'react';
import Link from 'umi/link';
import { Layout, Row, Col } from 'antd';
import BasicLayout from '../layouts/BasicLayout';
import ProfileCard from '@/components/ProfileCard';
import FinanceCard from '@/components/FinanceCard';
import DrawerMenu from '@/components/DrawerMenu';
import { config } from '@/global';

interface Props {
  welcome: any,
  profile: any,
}

export default class ProfilePage extends React.Component<Props, {}>{
  render() {
    const props = this.props;
    const content = props.welcome.content;
    const profile = props.profile;
    const transactions = props.profile.transactions;
    return (
      <BasicLayout title={"Profile"}>
        <Layout.Content style={{ marginTop: 64, textAlign: 'center' }}>
        <Row>
          <Col md={4}>&nbsp;</Col>
          <Col md={20}>
            <Row style={{ height: 300 , paddingTop: 50, paddingBottom: 50 }}>
                {content}
            </Row>
            <Row style={{ height: 300 }}>
              <ProfileCard profile={profile} />
            </Row>
            <Row style={{ height: 300 }}>
              <FinanceCard transactions={transactions} />
            </Row>
          </Col>
        </Row>
        </Layout.Content>
      </BasicLayout>
    );

  }
}
