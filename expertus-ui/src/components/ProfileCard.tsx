import * as React from 'react';
import { Layout, Row, Col, Card } from 'antd';
import Link from 'umi/link';

interface Props{
  profile: any,
}
const { Meta } = Card;
export default class ProfileCard extends React.Component<Props, {}>{
  render() {
      const props = this.props;

      return (
          <Row id='profile'>
            <Card style={{ width: 300, textAlign: 'left', borderColor: 'gray' }}>
            <Meta
              title="User Profile"
            />
            <p>&nbsp;</p>
            <p>Name: {props.profile.name}</p>
            <p>Email: {props.profile.email}</p>
            </Card>
          </Row>
      );

  }
}
