import * as React from 'react';
import Link from 'umi/link';
import { Layout, Row, Icon,Col } from 'antd';
import BasicLayout from '../layouts/BasicLayout';

interface Props{}
export default class HomePageSkeleton extends React.Component<Props, {}>{
  render() {
      return (
        <BasicLayout title={"Home"}>
          <Layout.Content style={{ marginTop: 64, textAlign: 'center' }}>
            <Row type="flex" style={{ height: 518, textAlign: 'center' }}>
                <Col md={8}>&nbsp;</Col>
                <Col md={8}><Icon type="loading" /> LOADING ....</Col>
                <Col md={8}>&nbsp;</Col>
            </Row>
          </Layout.Content>
        </BasicLayout>
      );

  }
}
