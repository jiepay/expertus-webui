import withRouter from 'umi/withRouter';
import NavBar from './NavBar';

interface Props{
  location?: {pathname: string},
}

const Header = (props: Props) => (
  <NavBar pathname={props.location.pathname} />
)

export default withRouter(Header)
