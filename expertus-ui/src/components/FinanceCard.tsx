import * as React from 'react';
import { Layout, Row, Col, Table } from 'antd';

interface Props{
  transactions: [],
}

export default class FinanceCard extends React.Component<Props, {}>{
  render() {
      const dataSource = this.props.transactions;
      const columns = [{
        title: 'Company',
        dataIndex: 'origin',
        key: 'origin',
      }, {
        title: 'Amount',
        dataIndex: 'amount',
        key: 'amount',
      }, {
        title: 'Type',
        dataIndex: 'transactionType',
        key: 'transactionType',
      }
      ,{
        title: 'Country',
        dataIndex: 'country',
        key: 'country',
      }];

      return (
            <Row id="finance">
                <Table dataSource={dataSource} columns={columns}/>
            </Row>
      );

  }
}
