import * as React from 'react';
import HomePage from '@/components/HomePage';
import axios from 'axios';

interface Props { }
interface State { }

export default class Home extends React.Component<Props, State>{

  render(){
    const profile = "/profile/";
    const email = ['hwatts@nullpod.com','sgreer@nullpod.com'];
    return (

      <HomePage profile={profile} email={email}/>
    )
  }
}
