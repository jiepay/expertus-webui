# README #

Web UI that will call the services of the expertus-backend


### How do I get set up? ###

* To run the project, install the yarn package manager (use)
    instructions at: https://linuxize.com/post/how-to-install-yarn-on-ubuntu-18-04/
* Go to the *expertus-ui* folder (it's easy to miss that one)
* Type *yarn* on the command line to download the dependencies of the project
* Type *yarn start* to run the project
* The UI by default runs on http://localhost:8080/

### Languages and Tools
1. Visual Studio Code IDE on Ubuntu 18.04
2. TypeScript 
3. ReactJs 
4. Antd for UI Components
5. UmiJs for basic routing (https://umijs.org/guide/)
6. Axios (for calling Webapis) (https://alligator.io/react/axios-react/)

### Who do I talk to? ###

jp
eternalsoul@gmail.com